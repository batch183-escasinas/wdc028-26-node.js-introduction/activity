const http = require("http");
const port = 3000;

http.createServer((request, response) => {
	if(request.url == '/login'){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Welcome to login page.");
	}else{
		// Set a status code for the response - a 404 means Not Found
		response.writeHead(404, {"Content-Type": "text/plain"})
		response.end("I'm sorry the page you are looking for cannot be found.");
	}
}).listen(port);

console.log(`server is successfully running in port: ${port}`);